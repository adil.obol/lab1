package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        if (row >= 0 && row < grid.size()) grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        if (grid == null || grid.size() == 0) {
            return true;
        }
    
        
        int rowSum = 0, colSum = 0;
        for (int i = 0; i < grid.get(0).size(); i++) {
            rowSum += grid.get(0).get(i);
            colSum += grid.get(i).get(0);
        }
    
        
        for (ArrayList<Integer> row : grid) {
            int sum = 0;
            for (int num : row) {
                sum += num;
            }
            if (sum != rowSum) {
                return false;
            }
        }
    
        
        for (int i = 0; i < grid.get(0).size(); i++) {
            int sum = 0;
            for (ArrayList<Integer> row : grid) {
                sum += row.get(i);
            }
            if (sum != colSum) {
                return false;
            }
        }
    
        return true;
    }
    

}