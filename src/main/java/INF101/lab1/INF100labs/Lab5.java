package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> myList = new ArrayList<>();
        for (int num : list){
            myList.add(num * 2);
        }
        return myList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        for (int i = list.size() - 1; i >= 0 ; i--){
            if (list.get(i) == 3) {
                list.remove(i);
            }
        }
        return list;

    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> onlyList = new ArrayList<>();
        ArrayList<Integer> sawList = new ArrayList<>();
        for (int num : list ) {
            if (!sawList.contains(num)){
                sawList.add(num);
                onlyList.add(num);
            }
        }

        return onlyList;

        
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++){
            a.set(i , a.get(i) + b.get(i));
        }
        
    }

}